﻿<%@ Page Title="Master Material Group" Language="C#" MasterPageFile="~/AjinomotoColorAdminNested.master" AutoEventWireup="true" CodeBehind="MasterMaterialGroup.aspx.cs" Inherits="AjinomotoWorkflowApp.MasterData.MasterMaterialGroup" %>

<%--css head--%>
<asp:Content ContentPlaceHolderID="css" runat="server">
    <link href="/Assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <style>
        .modal {
            overflow-y: auto;
        }
    </style>
</asp:Content>
<%--end css head--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="searchData" placeholder="Search Material Group Code, Name, Lead Time or Is Need Split">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="col-md-12" style="padding-bottom: 15px;">
                <div class="col-md-3 pull-right">
                    <a class="btn btn-primary pull-right" href="#addMaterialGroup" data-toggle="modal"><span class="fa fa-plus"></span> &nbsp;Add</a>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-condensed data-table">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Lead Time</th>
                            <th>Is Need Split</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>0110450</td>
                            <td>Garam</td>
                            <td>16</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110300</td>
                            <td>Adamyl</td>
                            <td>30</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110290</td>
                            <td>Cellotape 1/2 Inch</td>
                            <td>30</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110280</td>
                            <td>Chilli Powder STP</td>
                            <td>60</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110270</td>
                            <td>BAY LEAVES POWDER</td>
                            <td>30</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110260</td>
                            <td>PVC Print</td>
                            <td>30</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110250</td>
                            <td>Garlic</td>
                            <td>45</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110240</td>
                            <td>Caramel Color</td>
                            <td>15</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110220</td>
                            <td>Tepung Sagu</td>
                            <td>30</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>0110210</td>
                            <td>Oyster Juice</td>
                            <td>45</td>
                            <td>True</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="arrow"></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                        <li>
                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Manage Material</button></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addMaterialGroup" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add Material Group</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <legend>Entry Area</legend>
                            <div class="form-group col-md-12">
                                <label class="col-md-3">Code</label>
                                <div class="col-md-8">
                                    <input type="text" value="" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-3">Name</label>
                                <div class="col-md-8">
                                    <input type="text" value="" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-3">Load Time</label>
                                <div class="col-md-8">
                                    <input type="text" value="" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-3">Is Need Split</label>
                                <div class="col-md-8">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="button" class="btn btn-primary" id="saveMaterialGroup">
                                    <span class="fa fa-save"></span>&nbsp;Save
                                </button>
                                <a href="javascript:;" class="btn btn-warning" data-dismiss="modal"><span class=""></span>Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<%--end content--%>

<%-- start script --%>
<asp:Content ContentPlaceHolderID="script" runat="server">
    <script src="/Assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/Assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/Assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/MasterData/js/master-material-group.js"></script>
</asp:Content>
<%--end script head--%>
