﻿<%@ Page Title="Master Document" Language="C#" MasterPageFile="~/AjinomotoColorAdminNested.master" AutoEventWireup="true" CodeBehind="MasterDocument.aspx.cs" Inherits="AjinomotoWorkflowApp.MasterData.MasterDocument" %>

<%--css head--%>
<asp:Content ContentPlaceHolderID="css" runat="server">
    <link href="/Assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <style>
        .modal {
            overflow-y: auto;
        }
    </style>
</asp:Content>
<%--end css head--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">
    <div class="panel-body panel-form">
        <div class="col-md-12">
            <div class='row'>
                <div class='col-md-12'>
                    <div class="form-horizontal">
                        <legend style="padding: 15px 0;">Master Document</legend>
                        <div class="col-md-12">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="searchData" placeholder="Search Material Group Code, Name, Lead Time or Is Need Split">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="col-md-12">
                            <a class="btn btn-primary pull-right" href="#addDocument" data-toggle="modal"><span class="fa fa-plus"></span>&nbsp; Add</a>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <br />
                                <table id="data-table" class="table table-striped table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="odd">
                                            <td>1</td>
                                            <td>Certificate of Weight in duplicate</td>
                                            <td class="text-center">
                                                <div class="btn-group text-center">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="even">
                                            <td>2</td>
                                            <td>Health Certificate in Duplicate</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>14</td>
                                            <td>Certificate of Origin (Form D) n triplicate</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="even">
                                            <td>15</td>
                                            <td>Bill of Lading in triplicate</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>16</td>
                                            <td>Insurance Policy in duplicate</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="even">
                                            <td>17</td>
                                            <td>Original Free Sale</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>18</td>
                                            <td>Others document if any</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="even">
                                            <td>19</td>
                                            <td>COA include 6 items heavy metal</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>20</td>
                                            <td>Certificate of Analysis in duplicate (include 6 item Heavy metal,As,Pb,Hg, Cu,Zn,Fe) & specification</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="even">
                                            <td>21</td>
                                            <td>Certificate of Origin (JIEPA) in triplicate</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="arrow"></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-trash"></span>&nbsp;Delete</button></li>
                                                        <li>
                                                            <button class="btn btn-default"><span class="fa fa-edit"></span>&nbsp;Edit</button></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addDocument" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add New Master Document</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-3">Code</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" id="documentCode" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-3">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" id="documentName" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" id="saveDocument"><span class="fa fa-save"></span>&nbsp; Save</button>
                                            <a href="javascript:;" class="btn btn-warning" data-dismiss="modal"><span class=""></span>Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<%--end content--%>

<%-- start script --%>
<asp:Content ContentPlaceHolderID="script" runat="server">
    <script src="/Assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/Assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/Assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/MasterData/js/master-document.js"></script>
</asp:Content>
<%--end script head--%>