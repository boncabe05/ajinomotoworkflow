﻿<%@ Page Title="Master Historical Unit Price" Language="C#" MasterPageFile="~/AjinomotoColorAdminNested.master" AutoEventWireup="true" CodeBehind="MasterHistoricalUnitPrice.aspx.cs" Inherits="AjinomotoWorkflowApp.MasterData.MasterHistoricalUnitPrice" %>

<%--css head--%>
<asp:Content ContentPlaceHolderID="css" runat="server">
    <link href="/Assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <style>
        .modal {
            overflow-y: auto;
        }
    </style>
</asp:Content>
<%--end css head--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">
    <div class="panel-body panel-form">
        <div class="col-md-12">
            <div class='row'>
                <div class='col-md-12'>
                    <div class="form-horizontal">
                        <legend style="padding: 15px 0;">Master Document</legend>
                        <div class="col-md-12">
                            <a class="btn btn-primary pull-right" href="#addDocument" data-toggle="modal"><span class="fa fa-plus"></span>&nbsp; Add</a>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <br />
                                <table id="data-table" class="table table-striped table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Supplier ID</th>
                                            <th>Supplier Name</th>
                                            <th>Address</th>
                                            <th>Telephone</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="odd">
                                            <td>1</td>
                                            <td>1250001185</td>
                                            <td>SUNGAI BUDI, PT</td>
                                            <td>GEDUNG WISMA BUDI</td>
                                            <td>021-5213383</td>
                                            <td>
                                                <button class='btn btn-default'>Manage Unit Price</button>
                                            </td>
                                        </tr>
                                        <tr class="even">
                                            <td>2</td>
                                            <td>1000001310</td>
                                            <td>AJINO</td>
                                            <td>GEDUNG WISMA BUDI</td>
                                            <td>021-5213383</td>
                                            <td>
                                                <button class='btn btn-default'>Manage Unit Price</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary">Map New Supplier</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addDocument" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add New Master Document</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-3">Code</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" id="documentCode" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-3">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" id="documentName" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" id="saveDocument"><span class="fa fa-save"></span>&nbsp; Save</button>
                                            <a href="javascript:;" class="btn btn-warning" data-dismiss="modal"><span class=""></span>Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<%--end content--%>

<%-- start script --%>
<asp:Content ContentPlaceHolderID="script" runat="server">
    <script src="/Assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/Assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/Assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/MasterData/js/master-historical-unit-price.js"></script>
</asp:Content>
<%--end script head--%>