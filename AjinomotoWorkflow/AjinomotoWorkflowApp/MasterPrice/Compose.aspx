﻿<%@ Page Title="Compose" Language="C#" MasterPageFile="~/AjinomotoColorAdminApp.master" AutoEventWireup="true" CodeBehind="Compose.aspx.cs" Inherits="AjinomotoWorkflowApp.MasterPrice.Compose" %>

<%--css head--%>
<asp:Content ContentPlaceHolderID="css" runat="server">
    <link href="/Assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="/Assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <link href="/Assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/css/bwizard.css" rel="stylesheet" />
    <style>
        .wizard-desc {
            //overflow:auto;
        }

            .wizard-desc ol li {
                font-size: 0.6em;
            }

        .bwizard-steps li a {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
        select[readonly].select2 + .select2-container {
            pointer-events: none;
            touch-action: none;
            .select2-selection {
                background: #eee;
                box-shadow: none;
            }
            .select2-selection__arrow, .select2-selection__clear {
                display: none;
            }
        }
    </style>
</asp:Content>
<%--end css head--%>


<%--content--%>
<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">
    <div class="wrapper ">
        <div class="panel-body bg-white">
            <div class="wizard-desc bwizard clearfix panel">
                <ol class="bwizard-steps clearfix " role="tablist">
                    <li role="tab" aria-selected="true" class="active" style="z-index: 7;">
                        <span class="label badge-inverse">1</span><br />
                        <a href="" class="hidden-phone">Compose </a>
                        <small>&nbsp;</small>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 6;">
                        <span class="label badge-inverse">2</span><br />
                        <a href="" class="hidden-phone">Section Manager Approval </a>
                        <small>&nbsp;</small>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 5;">
                        <span class="label badge-inverse">3</span><br />
                        <a href="" class="hidden-phone">Department Manager Approval</a>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 4;">
                        <span class="label badge-inverse">4</span><br />
                        <a href="" class="hidden-phone">General Manager Approval</a>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 3;">
                        <span class="label badge-inverse">5</span><br />
                        <a href="" class="hidden-phone">Director Approval</a>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 2;">
                        <span class="label badge-inverse">6</span><br />
                        <a href="" class="hidden-phone">Vice President Approval</a>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 1;">
                        <span class="label badge-inverse">7</span><br />
                        <a href="" class="hidden-phone">President Director Approval</a>
                    </li>
                    <li role="tab" aria-selected="false" class="" style="z-index: 1;">
                        <br />
                        <a href="" class="hidden-phone">Finish</a>
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="panel panel-inverse" data-sortable-id="">
                    <div class="panel-heading">
                        <h4 class="panel-title">Requestor Data</h4>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="name">Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="name" placeholder="" value="DEDY ARIFIANTO" readonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="company-name">Company Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="company-name" placeholder="" value="Ajinomoto Indonesia" readonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="department-name">Department Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="department-name" placeholder="" value="Strategic Sourcing Dept" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-inverse" data-sortable-id="">
                    <div class="panel-heading">
                        <h4 class="panel-title">Master Price</h4>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <fieldset>
                                <!-- <legend>Requestor Data</legend> -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="company-name2">Company</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="company-name2" readonly>
                                                    <optgroup label="Company">
                                                        <option value="ajinomoto">Ajinomoto Indonesia</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="factory">Factory</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="factory">
                                                    <optgroup label="Factory">
                                                        <option value="mojokerto">Mojokerto</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="section-name">Section Name</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="section-name" readonly>
                                                    <optgroup label="Section">
                                                        <option value="ppc-sajiku-fac">PPC Sajiku - FAC</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="section-name">Supplier</label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control browse-supplier" placeholder="" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-secondary btn-add-supplier" type="button">
                                                            <i class="fa fa-ellipsis-h"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="po-type">PO Type</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="po-type" readonly>
                                                    <optgroup label="PO TYPE">
                                                        <option value="ajinomoto">SP Domestic</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="currency">Currency</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="currency">
                                                    <optgroup label="Currency">
                                                        <option value="idr">IDR - Rupiah</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="attachment">Attachment</label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" readonly="">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary">
                                                            Browse… <input type="file" style="display: none;" multiple="">
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3" for="remarks">Remarks</label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" id="remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-condensed" id="table-add-master-price">
                                                <thead>
                                                    <tr>
                                                        <th style="">Material</th>
                                                        <th>UOM</th>
                                                        <th>Quantity</th>
                                                        <th>Unit Price</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button type="button" class="btn btn-primary m-r-5">Submit</button>
                                        <button type="button" class="btn btn-default" id="save_as_draft">Save as Draft</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseMaterial" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">List Of Material</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status Material</label>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" name="optionsRadios" value="all" checked />
                                        All Material
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="optionsRadios" value="exist" />
                                        Already Exist in this Master Price
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="optionsRadios" value="not_exist" />
                                        Not Exist in this Master Price
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table id="data-table" class="table table-striped table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>Material ID</th>
                                                <th>Material Name</th>
                                                <th>Material Category ID</th>
                                                <th>Material Category Name</th>
                                                <th>UOM</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseSupplier" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">List Of Supplier</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table id="browse-supplier" class="table table-striped table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>Supplier Code</th>
                                                <th>Supplier Name</th>
                                                <th>Payment Term ID</th>
                                                <th>Payment Term Description</th>
                                                <th>Category</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<%--end content--%>

<%-- start script --%>
<asp:Content ContentPlaceHolderID="script" runat="server">
    <script src="/Assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/Assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/Assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/Assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/Assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/masterPrice/js/compose.js"></script>
</asp:Content>
<%--end script head--%>