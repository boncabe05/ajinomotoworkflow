﻿$("#menu-master-price").addClass('active');
$("#menu-master-price").parent().parent().addClass('active');
$("#company-name2").select2({ placeholder: "Select a Company" });
//$("#department-name").select2({ placeholder: "Select a Department" });
$("#section-name").select2({ placeholder: "Select a Section" });
$("#factory").select2({ placeholder: "Select a Factory" });
$("#status-request").select2({ placeholder: "Select a Status" });
$("#po-type").select2({ placeholder: "Select a PO Type" });
$("#currency").select2({ placeholder: "Select a Currency" });

$('.datepicker-request-date').datepicker({
    autoclose: true,
    showButtonPanel: true,
    format: "dd-mm-yyyy",
    startView: "days"
});

$('.datepicker-request-period').datepicker({
    autoclose: true,
    showButtonPanel: true,
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});

var table_material = $('#data-table').DataTable({
    "ajax": "js/ajax_browse_material.json",
    "columnDefs": [{
        "processing": true,
        "serverSide": true,
        "targets": -1,
        "data": null,
        "defaultContent": "<button class='btn btn-default'>Select</button>"
    }]
});

var table_supplier = $('#browse-supplier').DataTable({
    "ajax": "js/ajax_browse_supplier.json",
    "columnDefs": [{
        "processing": true,
        "serverSide": true,
        "targets": -1,
        "data": null,
        "defaultContent": "<button class='btn btn-default select-supplier'>Select</button>"
    }]
});

var table = $("#table-add-master-price").DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "filter": false,
    "autoWidth": false,
    "columns": [{
        "name": "material",
        "targets": 0,
        "render": function (data, type, row, meta) {
            var html = '<div class="col-md-12"><div class="form-group"><div class="input-group"><input type="text" style="width:100px" class="form-control material" placeholder="" disabled><span class="input-group-btn"><button class="btn btn-secondary btn-add-material" type="button"><i class="fa fa-ellipsis-h"></i></button></span></div></div></div>';
            return html;
        }
    }, {
        "name": "uom",
        "targets": 1,
        "render": function (data, type, row, meta) {
            var html = '<div class="col-md-12"><div class="form-group"><div class="input-group"><input type="text" class="form-control uom" style="width:80px" placeholder="" disabled><span class="input-group-btn"><button class="btn btn-secondary btn-add-uom" type="button"><i class="fa fa-ellipsis-h"></i></button></span></div></div></div>';
            return html;
        }
    }, {
        "name": "quantity",
        "targets": 2,
        "render": function (data, type, row, meta) {
            var html = '<div class="col-md-12"><div class="form-group"><input type="text" class="form-control input-add-quantity" placeholder="" style="width:80px"></div></div>';
            return html;
        }
    }, {
        "name": "unit_price",
        "targets": 3,
        "render": function (data, type, row, meta) {
            var html = '<div class="col-md-12"><div class="form-group"><input type="text" class="form-control input-add-unit-price" placeholder="" style="width:80px"></div></div>';
            return html;
        }
    }, {
        "name": "action",
        "targets": 4,
        "render": function (data, type, row, meta) {
            var html = '<div class="col-md-12"><div class="form-group" style="width:100px"><button class="btn btn-default remove-row"><i class="fa fa-minus"></i></button>&nbsp;<button class="btn btn-primary add-new"><i class="fa fa-plus"></i></button></div></div>';
            return html;
        }
    }]
});

$('#table-add-master-price').on('click', '.add-new', function (e) {
    e.preventDefault();
    $(this).hide();
    $('.remove-row').show();
    table.row.add({
        "material": '',
        "uom": '',
        "quantity": '',
        "unit_price": '',
        "action": ''
    }).draw();
});

//Add row Data Table
$('#table-add-master-price').on('click', '.remove-row', function (e) {
    e.preventDefault();
    var btn = $('button.add-new');
    var currRow = $(this).closest('tr').prev().children('td').find('.add-new').show();
    table.row($(this).parents('tr')).remove().draw();
});

//Browse Material
var material_ = null;
var materialVal = null;
var uomVal = null;
$('#table-add-master-price').on('click', '.btn-add-material', function (e) {
    e.preventDefault();
    materialVal = $(this).closest('tr').children('td').find('.material');
    uomVal = $(this).closest('tr').children('td').find('.uom');
    $('#browseMaterial').modal('show');
});

$('#data-table').on('click', '.btn', function (e) {
    e.preventDefault();
    var selected = table_material.row($(this).parents('tr')).data();
    var materialID = selected[0];
    var materialName = selected[1];
    material_ = materialID + '-' + materialName;
    materialVal.val(material_);
    uomVal.val(selected[4]);
    $('#browseMaterial').modal('hide');
});


//Browse Supplier
$('.btn-add-supplier').on('click', function (e) {
    $('#browseSupplier').modal('show');
});
$('#browse-supplier').on('click', '.select-supplier', function (e) {
    e.preventDefault();
    var selected = table_supplier.row($(this).parents('tr')).data();
    var supplierlID = selected[0];
    var supplierName = selected[1];
    supplier_ = supplierlID+ ' - ' + supplierName;
    $('.browse-supplier').val(supplier_);
    $('#browseSupplier').modal('hide');
});