﻿<%@ Page Title="Trash" Language="C#" MasterPageFile="~/AjinomotoColorAdminApp.master" AutoEventWireup="true" CodeBehind="Trash.aspx.cs" Inherits="AjinomotoWorkflowApp.MasterPrice.Trash" %>

<%--css head--%>
<asp:Content ContentPlaceHolderID="css" runat="server">
    <link href="/Assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<%--end css head--%>


<%--content--%>
<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">
    <div class="wrapper bg-silver text-center" style="padding: 15px 5px 10px !important;">
        <form>
            <div class="input-group m-b-15">
                <input type="text" class="form-control input-sm input-white" placeholder="Search Mail" />
                <span class="input-group-btn">
                    <button class="btn btn-sm btn-inverse" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
    </div>
    <div class="email-content">
        <table class="table table-email" style="overflow: hidden" id="table-trash">
            <thead>
                <tr>
                    <th class="email-select"><a href="#" data-click="email-select-all"><i class="fa fa-square-o fa-fw"></i></a></th>
                    <th>Incident Number</th>
                    <th>Attachment</th>
                    <th>Process Name</th>
                    <th>Status</th>
                    <th>Created Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                    <td class="email-sender">Leap Motion
                    </td>
                    <td class="email-subject">
                        <a href="#" class="email-btn" data-click="email-archive"><i class="material-icons">attach_file</i></a>
                        Maecenas ultrices interdum leo, eu aliquam diam mattis sed.
                    </td>
                    <td></td>
                    <td></td>
                    <td class="email-date">11/4/2014</td>
                </tr>
                <tr>
                    <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                    <td class="email-sender">Leap Motion
                    </td>
                    <td class="email-subject">
                        <a href="#" class="email-btn" data-click="email-archive"><i class="material-icons">attach_file</i></a>
                        Ut tristique dapibus nibh, sed scelerisque magna vehicula a.
                    </td>
                    <td></td>
                    <td></td>
                    <td class="email-date">11/4/2014</td>
                </tr>
                <tr>
                    <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                    <td class="email-sender">Stefie Chin
                    </td>
                    <td class="email-subject">
                        <a href="#" class="email-btn" data-click="email-archive"><i class="material-icons">attach_file</i></a>
                        Etiam pretium neque vitae vulputate fermentum.
                    </td>
                    <td></td>
                    <td></td>
                    <td class="email-date">11/4/2014</td>
                </tr>
            </tbody>
        </table>
        <div class="email-footer clearfix">
            737 messages
            <ul class="pagination pagination-sm m-t-0 m-b-0 pull-right">
                <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-double-left"></i></a></li>
                <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-left"></i></a></li>
                <li><a href="javascript:;"><i class="fa fa-angle-right"></i></a></li>
                <li><a href="javascript:;"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
    </div>
</asp:Content>
<%--end content--%>

<%-- start script --%>
<asp:Content ContentPlaceHolderID="script" runat="server">
    <script src="/Assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/Assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/Assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/masterPrice/js/trash.js"></script>
</asp:Content>
<%--end script head--%>