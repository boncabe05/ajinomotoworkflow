﻿<%@ Page Title="Report PO Raw Material" Language="C#" MasterPageFile="~/AjinomotoColorAdminNested.master" AutoEventWireup="true" CodeBehind="PORawMaterial.aspx.cs" Inherits="AjinomotoWorkflowApp.Report.PORawMaterial" %>

<%--css head--%>
<asp:Content ContentPlaceHolderID="css" runat="server">
    <link href="/Assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="/Assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="/Assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
</asp:Content>
<%--end css head--%>


<%--content--%>
<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">
    <div class="panel-body">
        <form class="">
            <div class="col-md-12">
                <div>
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Material</label>
                                    <select class="select2-material form-control">
                                        <optgroup label="MaterialName">
                                            <option value="name">Material Name</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <select class="select2-supplier form-control">
                                        <optgroup label="SupplierName">
                                            <option value="name">Supplier Name</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Month Start</label>
                                    <div class="input-group date datepicker-range-start" data-date-format="dd-mm-yyyy" >
                                        <input type="text" class="form-control" placeholder="Select Date" id="datepicker-start" />
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Month End</label>
                                    <div class="input-group date datepicker-range-end" data-date-format="dd-mm-yyyy" >
                                        <input type="text" class="form-control" placeholder="Select Date" id="datepicker-end" />
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <div>
                                        <button class="btn btn-primary btnGenerateRangeRaw">Generate</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-12" id="reportBody" style="display: none">
                    <div class="text-center">
                        <h2>Report PO Raw Material</h2>
                        <h3 id="monthReport"></h3>
                    </div>
                    <br />
                    <div class="col-md-12" id="reportContent">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Email Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Nicky Almera</td>
                                    <td>nicky@hotmail.com</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Edmund Wong</td>
                                    <td>edmund@yahoo.com</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Harvinder Singh</td>
                                    <td>harvinder@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Terry Khoo</td>
                                    <td>terry@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Camryn Wong</td>
                                    <td>camryn@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Lelouch Wong</td>
                                    <td>lelouch@gmail.com</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
<%--end content--%>

<%-- start script --%>
<asp:Content ContentPlaceHolderID="script" runat="server">
    <script src="/Assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/Assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/Report/js/po-raw-material.js"></script>
</asp:Content>
<%--end script head--%>
