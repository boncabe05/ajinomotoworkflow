﻿$(".select2-company").select2({ placeholder: "Select a Company" });
$(".select2-user").select2({ placeholder: "Select a User" });

$('.datepicker-start').datepicker({
    autoclose: true,
    showButtonPanel: true,
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});
$('.btnGenerate').on('click', function (e) {
    e.preventDefault();
    var date_ = $('#datepicker').val();
    $('#monthReport').html(date_);
    $('#reportBody').show();
});