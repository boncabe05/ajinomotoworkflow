﻿$(".select2-supplier").select2({ placeholder: "Select a Supplier" });

$('.datepicker-range-start').datepicker({
    autoclose: true,
    showButtonPanel: true,
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});
$('.datepicker-range-end').datepicker({
    autoclose: true,
    showButtonPanel: true,
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});
$('.btnGenerateRange').on('click', function (e) {
    e.preventDefault();
    var date_start = $('#datepicker-start').val();
    var date_end = $('#datepicker-end').val();
    var date_ = "From : " + date_start + " to " + date_end;
    $('#monthReport').html(date_);
    $('#reportBody').show();
});